﻿const cakeControllerUri = "/api/cake/";
const reviewControllerUri = "/api/review/";

$(document).ready(function () {
    var cakeId = getUrlVars()["cakeid"];
    getCake(cakeId);
    getReviews();
});


function displayReviews(reviews) {
    $("#reviews").empty();

    var cakeid = getUrlVars()["cakeid"];
    $.each(reviews, function (key, review) {
        if (review.cakeId == cakeid) {
            $("#reviews").append('<div class="card card-outline-secondary my-4"><div class="card-header">Product Reviews<div class="card-body" id="commentText"><p>' + review.comment + '</p><hr><small class="text-muted"  id="mailText">' + review.mail + '</small></div></div></div>');
        }
    });

}

function getReviews() {
    $.ajax({
        type: "GET",
        url: reviewControllerUri,
        cache: false,
        success: function (reviews) {
            displayReviews(reviews);
        }
    });
}



function addReview() {
    var cakeId = getUrlVars()["cakeid"];
    var formData = new FormData();

    formData.append("Comment", $("#Comment").val());
    formData.append("Mail", $("#Mail").val());
    formData.append("CakeId", cakeId);



    $.ajax({
        type: "POST",
        url: reviewControllerUri + "PostReview",
        processData: false,
        contentType: false,
        data: formData,
        mimeType: "multipart/form-data",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            alert("review was added successfully!");
            location.reload();
        }
    });

    return false;
}


// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayCakeDetails(cake) {
    $('#cake-name').html(cake.name);
    $('#cake-price').html(cake.price + ' lei');
    $('#cake-description').html(cake.description);
    $('#cake-category').html(cake.category);
    $('#cake-image').css('max-height', '400px');
    $('#cake-image').attr("src", cake.base64Image);
}

function getCake(cakeId) {
    $.ajax({
        type: "GET",
        url: cakeControllerUri + cakeId,
        cache: false,
        success: function (data) {
            displayCakeDetails(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        }
    });
}